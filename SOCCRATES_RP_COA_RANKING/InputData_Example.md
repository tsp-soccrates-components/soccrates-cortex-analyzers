# Input Data Example for the Response Planner Coa Ranking cortex Analyzer.

## Example 1: Complete input
{"siem_label":"TA0001","initialTTC":{"228.Read":[19,26,34],"325.NetworkRespondConnect":[23,30,40]},"initial_ids":{"pid":"446664605265379","tid":"185259889010721","simid":"134974016992369"},"containment":false,"coas":[{"efficiency":"170.585","coaTTC":{"228.Read":[18,25,34],"325.NetworkRespondConnect":[17976931348623157,17976931348623157,17976931348623157]},"coa_ids":{"pid":"446664605265379","tid":"185259889010721","simid":"168109066230650"},"time_cost":{"1":355,"2":564,"3":22,"4":895,"5":1336},"monetary_cost":{"1":4464,"2":564,"3":556,"4":895,"5":1336},"defenses":[{"ref":"100","defensename":"Disabled","defenseInfo":"Info","mitreRef":"Ref"}]},{"efficiency":"272.808","coaTTC":{"228.Read":[17976931348623157,17976931348623157,17976931348623157],"325.NetworkRespondConnect":[17976931348623157,17976931348623157,17976931348623157]},"time_cost":{"1":355,"2":564,"3":22,"4":895,"5":1336},"monetary_cost":{"1":87355,"2":564,"3":228750,"4":8955,"5":8788},"defenses":[{"ref":"119","defensename":"authenticated","defenseInfo":"Info","mitreRef":"Ref"},{"ref":"100","defensename":"Disabled","defenseInfo":"Info","mitreRef":"Ref"}]}]}

## Example 2: Containment CoAs example
{"containment":true,"coas":[{"defenses":[{"ref":"112244","defenseName":"Isolate","defenseInfo":"NetworkSegmentation","mitreRef":"M1030","hosts":["192.168.0.3","192.168.0.10"]}]},{"defenses":[{"ref":"112244","defenseName":"Isolate","defenseInfo":"NetworkSegmentation","mitreRef":"M1030","hosts":["192.168.0.3","192.168.0.10"]},{"ref":"112255","defenseName":"Filter","defenseInfo":"FilterNetworkTraffic","mitreRef":"M1037","flows":[{"src":"192.168.0.3","dst":"123.45.67.89"}]}]}]}


## Example 3: Only optional siem_label field
{"siem_label":"TA0001","coas":[{"coaTTC":{"228.Read":[18,25,34],"325.NetworkRespondConnect":[17976931348623157,17976931348623157,17976931348623157]},"defenses":[{"ref":"100","defensename":"Disabled","defenseInfo":"Info","mitreRef":"Ref"}]},{"coaTTC":{"228.Read":[17976931348623157,17976931348623157,17976931348623157],"325.NetworkRespondConnect":[17976931348623157,17976931348623157,17976931348623157]},"defenses":[{"ref":"119","defensename":"authenticated","defenseInfo":"Info","mitreRef":"Ref"},{"ref":"100","defensename":"Disabled","defenseInfo":"Info","mitreRef":"Ref"}]}]}


## Example 4: Only optional initialTTC field
{"initialTTC":{"228.Read":[19,26,34],"325.NetworkRespondConnect":[23,30,40]},"coas":[{"coaTTC":{"228.Read":[18,25,34],"325.NetworkRespondConnect":[17976931348623157,17976931348623157,17976931348623157]},"defenses":[{"ref":"100","defensename":"Disabled","defenseInfo":"Info","mitreRef":"Ref"}]},{"coaTTC":{"228.Read":[17976931348623157,17976931348623157,17976931348623157],"325.NetworkRespondConnect":[17976931348623157,17976931348623157,17976931348623157]},"defenses":[{"ref":"119","defensename":"authenticated","defenseInfo":"Info","mitreRef":"Ref"},{"ref":"100","defensename":"Disabled","defenseInfo":"Info","mitreRef":"Ref"}]}]}



### Exemple 1: Readable version
{
  "siem_label":"TA0001",
  "initialTTC": {
       "228.Read": [
         19,
         26,
         34
       ],
       "325.NetworkRespondConnect": [
         23,
         30,
         40
       ]
    },
  "initial_ids": {
    "pid":"446664605265379",
    "tid":"185259889010721",
    "simid": "134974016992369"
  }
  "containment": false,
  "coas": [
  {
      "efficiency": "170.585",
      "coaTTC": {
        "228.Read": [
          18,
          25,
          34
        ],
        "325.NetworkRespondConnect": [
          1.7976931348623157e+308,
          1.7976931348623157e+308,
          1.7976931348623157e+308
        ]
      },
      "coa_ids": {
        "pid":"446664605265379",
        "tid":"185259889010721",
        "simid": "168109066230650"
      },
      "time_cost": {
        "1": 355,
        "2":564,
        "3":22,
        "4":895,
        "5":1336
      },
      "monetary_cost": {
          "1": 4464,
          "2":564,
          "3":556,
          "4":895,
          "5":1336     
      },
      "defenses": [
        {
          "ref": "100",
          "defensename": "Disabled",
          "defenseInfo": "Info",
          "mitreRef": "Ref"
        }
      ]
    },
    {
      "efficiency": "272.808",
      "coaTTC": {
        "228.Read": [
          1.7976931348623157e+308,
          1.7976931348623157e+308,
          1.7976931348623157e+308
        ],
        "325.NetworkRespondConnect": [
          1.7976931348623157e+308,
          1.7976931348623157e+308,
          1.7976931348623157e+308
        ]
      },
      "time_cost": {
          "1": 355,
          "2":564,
          "3":22,
          "4":895,
          "5":1336:
      },
      "monetary_cost": {
        "1": 87355,
        "2":564,
        "3":228750,
        "4":8955,
        "5":8788
      },
      "defenses": [
        {
          "ref": "119",
          "defensename": "authenticated",
          "defenseInfo": "Info",
          "mitreRef": "Ref"
        },
        {
          "ref": "100",
          "defensename": "Disabled",
          "defenseInfo": "Info",
          "mitreRef": "Ref"
        }
      ]
    }
  ]
}
