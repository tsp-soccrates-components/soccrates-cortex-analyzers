#!/usr/bin/env python3
# encoding: utf-8
import json
import time
import requests
import datetime

from cortexutils.analyzer import Analyzer


def logprint(message,data):
    date= datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
    filelog = open("/var/log/cortex_analyzers/business_Critical_analyser","a")
    filelog.write("[" + str(date) + "]" + "[" + message + "]" + str(data) + "\n")
    filelog.close()

class SOCCRATESCriticalAnalyzer(Analyzer):

    def __init__(self):
        Analyzer.__init__(self)
        self.service = self.get_param('config.service', None, 'Service parameter is missing')
        self.businessCriticalAnalyser_url = self.get_param('config.url', None, 'Missing URL of the Business Critical Analyser')
        self.criticality_path = self.get_param('config.criticality_service', None, 'Missing path of the critical service')

    def check_response(self, response):
        status=response.status_code
        if status == 400:
            logprint("Response Error",str(status) + " - " + str(response.content))
            self.error('Status: ' + str(status) + " - " + str(response.content))
        return response


    def sendRequest(self, service, data):
        if service == 'getCriticality':
            if 'threshold' in data:
                threshold = data['threshold']
                base_url = self.businessCriticalAnalyser_url + self.criticality_path
                url = base_url + '?threshold=' + str(threshold)
            elif 'Threshold' in data:
                threshold = data['Threshold']
                base_url = self.businessCriticalAnalyser_url + self.criticality_path
                url = base_url + '?threshold=' + str(threshold)
            else:
                base_url = self.businessCriticalAnalyser_url + self.criticality_path
                url = base_url
            request_headers = {'Content-Type': 'application/json', 'accept': 'application/json'}
            response = requests.post(url, headers=request_headers)
            self.check_response(response)
        else:
            response = None
        return response


    def run(self):
        if self.service == 'getCriticality':
            if self.data_type == 'other':
                inputData = self.get_param('data', None, 'Data is missing')
                logprint("input", inputData)
                logprint("Type", type(inputData))
                json_data = json.loads(inputData)
                response = self.sendRequest(service=self.service, data=json_data)
                self.report(json.loads(response.content))
            else:
                self.error('Invalid data type')

        else:
            self.error('Invalid service')


if __name__ == '__main__':
    SOCCRATESCriticalAnalyzer().run()
