#!/usr/bin/env python3
# encoding: utf-8
import json
import time
import requests
import datetime

from cortexutils.analyzer import Analyzer

def logprint(message,data):
    date= datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
    filelog = open("/var/log/cortex_analyzers/business_impact_analyser","a")
    filelog.write("[" + str(date) + "]" + "[" + message + "]" + str(data) + "\n")
    filelog.close()

class SOCCRATESImpactAnalyzer(Analyzer):

    def __init__(self):
        Analyzer.__init__(self)
        self.service = self.get_param('config.service', None, 'Service parameter is missing')
        self.businessImpactAnalyser_url = self.get_param('config.url', None, 'Missing URL of the Business Impact Analyser')
        self.probability_path = self.get_param('config.probability_service', None, 'Missing path of the probability service')

    def check_response(self, response):
        status=response.status_code
        if status == 400:
            logprint("Response Error",str(status) + " - " + str(response.content))
            self.error('Status: ' + str(status) + " - " + str(response.content))
        return response
    
    def sendRequest(self, service, data):
        if service == 'getProbability':
            attack_time = ''
            if 'attack_time' in data:
                attack_time = data['attack_time']
                base_url = self.businessImpactAnalyser_url + self.probability_path
                url = base_url + '?attack_time=' + str(attack_time)
            else:
                base_url = self.businessImpactAnalyser_url + self.probability_path
                url = base_url
            
            Impacted_assets = data['Impacted_assets']
            request_headers = {'Content-Type': 'application/json', 'accept': 'application/json'}
            
            if 'business_name' in data:
                business_name = data['business_name']
                Payload={"Impacted_assets":Impacted_assets,"business_name":business_name}

            else:
                Payload={"Impacted_assets":Impacted_assets}
             
            response = requests.post(url, json = Payload, headers=request_headers)
            self.check_response(response)
        else:
            response = None
        return response
        
        

    def run(self):
        if self.service == 'getProbability':
            if self.data_type == 'other':
                inputData = self.get_param('data', None, 'Data is missing')
                logprint("input", inputData)
                logprint("Type", type(inputData))
                json_data = json.loads(inputData)
                response = self.sendRequest(service=self.service, data=json_data)
                self.report(json.loads(response.content))
            else:
                self.error('Invalid data type')

        else:
            self.error('Invalid service')




if __name__ == '__main__':
    SOCCRATESImpactAnalyzer().run()
