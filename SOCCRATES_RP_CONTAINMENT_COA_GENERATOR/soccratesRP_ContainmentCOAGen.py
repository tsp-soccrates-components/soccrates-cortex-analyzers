#!/usr/bin/env python3
# encoding: utf-8
import datetime
import json
import time

from cortexutils.analyzer import Analyzer
import requests


def logprint(message,data):
    date = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
    filelog = open("/var/log/cortex_analyzers/cortex_Soccrates_RP_containment_coa_gen.log","a")
    filelog.write("[" + str(date) + "]" + "[" + message + "]" + str(data) + "\n")
    filelog.close()


class SOCCRATES_RP_CONTAINMENT_COA_GENERATOR_ANALYZER(Analyzer):
    def __init__(self):
        Analyzer.__init__(self)
        self.service = self.get_param('config.service', 'generateContainmentCoas',
                                      'Service parameter is missing')
        self.responsePlannerApi_url = self.get_param('config.responsePlannerApi_url',
                                                     'http://rp-backend:8000/',
                                                     'Missing URL of the Response Planner API')
        self.service_url = self.get_param('config.generateContainmentCoas_url',
                                          'generateContainmentCoas',
                                          'Missing url to the generateContainmentCoas service in the RP API')
        self.inputRequested = ['sourceIPs', 'destinationIPs']


    def check_response(self, response):
        status = response.status_code
        if status != 200:
            logprint("Response Error", str(status) + " - " + str(response.content))
            self.error('Status: ' +  str(status) + " - " + str(response.content))
        return response


    def check_inputData(self, inputData):
        for i in self.inputRequested:
            if not(i in inputData):
                logprint("Missing input:", i)
                self.error("Missing input:" + i)


    def generateUrl_Payload(self, inputData):
        siem_label = inputData.get('siem_label')
        type = inputData.get('type')
        sourceIPs = inputData.get('sourceIPs')
        destinationIPs = inputData.get('destinationIPs')
        json_payload = {"sourceIPs": sourceIPs, "destinationIPs": destinationIPs}
        url = f'{self.responsePlannerApi_url}{self.service_url}?'
        if isinstance(type, str):
            url += f'type={type}&'
        if isinstance(siem_label, str):
            url += f'siem_label={siem_label}'
        return url, json_payload


    def sendRequestToAPI(self, url, json_payload):
        logprint('Request URL', url)
        request_headers = {'Content-Type': 'application/json',
                           'accept': 'application/json'}
        response = requests.post(url, json=json_payload, headers=request_headers)
        self.check_response(response)
        return response


    def run(self):
        if self.service == 'generateContainmentCoas':
            if self.data_type == 'other':
                raw_inputData = self.get_param('data', None, 'Data is missing')
                inputData = json.loads(raw_inputData)
                logprint("input", inputData)
                self.check_inputData(inputData)
                url, json_payload = self.generateUrl_Payload(inputData)
                response = self.sendRequestToAPI(url, json_payload)

                logprint("Response from the Response Planner API",
                        response.content)
                self.report(json.loads(response.content))
            else:
                self.error('Invalid data type')
        else:
            self.error('Invalid service')


# Example of data
SIEM_EVENT_EXAMPLE = "TA0001" # Initial Access
SOURCE_IPS_EXAMPLE = ["192.168.0.3", "192.168.0.10"]
DESTINATION_IPS_EXAMPLE = ["123.45.67.89", "123.98.76.54"]

CONTAINMENT_COA_GEN_INPUT_EXAMPLE = {"siem_label": SIEM_EVENT_EXAMPLE,
                                     "sourceIPs": SOURCE_IPS_EXAMPLE,
                                     "destinationIPs": DESTINATION_IPS_EXAMPLE}


# Launch the Analyzer
if __name__ == '__main__':
    SOCCRATES_RP_CONTAINMENT_COA_GENERATOR_ANALYZER().run()
