# Input Data Example for the Response Planner Containment coa generator cortex Analyzer.

## Example 1: UC 1 - Final Containment phase - containment
{"type":"containment", "sourceIPs": ["192.168.0.3", "192.168.0.10"], "destinationIPs": ["123.45.67.89", "123.98.76.54"], "siem_label": "TA0001"}

### Readable version
{
  "type": "containment",
  "sourceIPs": [
    "192.168.0.3",
    "192.168.0.10"
  ],
  "destinationIPs": [
    "123.45.67.89",
    "123.98.76.54"
  ],
  "siem_label": "TA0001"
}

### Output 1 expected:
{
  "containment": true,
  "coas": [
    {
      "defenses": [
        {
          "defenseName": "Host isolation",
          "defenseInfo": "Isolate the host from the network by using Network Segmentation",
          "hosts": [
            "192.168.0.10",
            "192.168.0.3"
          ],
          "mitreRef": "M1030"
        }
      ]
    }
  ]
}



## Example 2: UC 1 - Preliminary phase - stop_exfiltration
{"type": "stop_exfiltration","sourceIPs": ["192.168.0.3", "10.18.22.44", "192.168.0.10"], "destinationIPs": ["123.45.67.89", "123.98.76.54"], "siem_label": "TA0010"}

### Output 2 expected:
{
  "containment": true,
  "coas": [
    {
      "defenses": [
        {
          "defenseName": "Traffic filtering",
          "defenseInfo": "Use network appliances to filter ingress or egress traffic and perform protocol-based filtering",
          "flows": [
            {
              "src": "192.168.0.10",
              "dst": "123.98.76.54"
            },
            {
              "src": "192.168.0.10",
              "dst": "123.45.67.89"
            },
            {
              "src": "192.168.0.3",
              "dst": "123.98.76.54"
            },
            {
              "src": "192.168.0.3",
              "dst": "123.45.67.89"
            }
          ],
          "mitreRef": "M1037"
        }
      ]
    }
  ]
}
